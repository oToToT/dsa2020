#pragma GCC optimize("Ofast,no-math-errno")
#include <stdio.h>
#include <inttypes.h>
typedef uint32_t u32;
#define N (1000000 + 5)
#define INF (((u32)1)<<31)

#define max(a, b) (((a)>(b))?(a):(b))
#define min(a, b) (((a)<(b))?(a):(b))

static inline char gc() {
    static char buf[100000], *p = buf, *end = buf;
    if (p == end) {
        end = buf + read(0, buf, 100000);
        p = buf;
    }
    return *p++;
}
static inline void gn(u32* x) {
    *x = 0;
    register char c = gc();
    while ('0'>c || c>'9')
        c = gc();
    while ('0'<=c && c<='9')
        *x = *x * 10 + c - '0', c=gc();
}

char pbuf[220000], *p = pbuf, *end = pbuf + 220000;
static inline void pc(char c) {
    *(p++) = c;
    if (p == end) {
        write(1, pbuf, 220000);
        p = pbuf;
    }
}
static inline void pflush() {
    write(1, pbuf, p - pbuf);
}
static inline void pn(u32 x) {
    static char bf[20], *bt = bf;
    if (!x) pc('0');
    else {
        for (;x;x/= 10) *(bt++) = x%10 + '0';
        while(bt != bf) pc(*(--bt));
    }
}

u32 s[N];
int r[N];
int b[N];

typedef struct Obj {
    int v, p;
} Obj;
Obj stk[N];

int main() {
    int n; gn(&n);
    for (int i = 1; i <= n; ++i)
        gn(&s[i]);
    for (int i = 1; i <= n; ++i)
        gn(&r[i]);

    register int stk_ = 0;
    stk[stk_].v = INF;
    stk[stk_++].p = n + 1;
    for (int i = n; i >= 1; --i) {
        while (stk[stk_ - 1].v < s[i])
            -- stk_;
        b[i] = min(i + r[i] + 1, stk[stk_ - 1].p) - 2;
        stk[stk_].v = s[i];
        stk[stk_++].p = i;
    }

    stk_ = 0;
    stk[stk_ ++].p = 0;
    for (int i = 1; i <= n; ++i) {
        while (stk[stk_ - 1].v < s[i])
            -- stk_;
        pn(max(i - r[i] - 1, stk[stk_ - 1].p));
        pc(' '); pn(b[i]); pc('\n');
        stk[stk_].v = s[i];
        stk[stk_++].p = i;
    }
    pflush();
    return 0;
}


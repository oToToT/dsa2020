#include"akihabara.h"
#include<stdio.h>
#include<stdlib.h>

static inline int gc() {
    static char buf[ 1 << 20 ], *p = buf, *end = buf;
    if ( p == end ) {
        end = buf + fread( buf, 1, 1 << 20, stdin );
        if ( end == buf ) return EOF;
        p = buf;
    }
    return *p++;
}
static inline void gn( long long *_ ) {
    register int c = gc(); register long long __ = 1; *_ = 0;
    while(('0'>c||c>'9') && c!=EOF && c!='-') c = gc();
    if(c == '-') { __ = -1; c = gc(); }
    while('0'<=c&&c<='9') *_ = *_ * 10 + c - '0', c = gc();
    *_ *= __;
}



char status=0;
unsigned int getN(){
    if(status!=0){
        fprintf(stderr,"call getN in wrong order!");
        exit(1);
    }
    status=1;
    long long N;
    gn(&N);
    return N;
}
long long int getK(){
    if(status!=1){
        fprintf(stderr,"call getK in wrong order!");
        exit(1);
    }
    status=2;
    long long int K;
    gn(&K);
    return K;
}
void getArray(unsigned int N,int array[]){
    if(status!=2){
        fprintf(stderr,"call getArray in wrong order!");
        exit(1);
    }
    status=3;
    for(unsigned int i=0;i<N;i++) {
        long long tmp;
        gn(&tmp);
        array[i]=tmp;
    }
    return;
}

#pragma GCC opimize("Ofast,no-math-errno,no-stack-protector,unroll-loops")
#include "akihabara.h"
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#define N (50000001)
#define M (286032011)

static int a[N];

static uint32_t en[M];
static int pt = 2;
static int nt[N];
static int v[N];
static int64_t h[N];

static inline uint64_t h1(uint64_t);
static inline int uGet(int64_t);
static inline void uAdd(int64_t);

int main() {
    uint32_t n = getN();
    int64_t k = getK();
    getArray(n, a);

    en[0] = 1;
    v[1] = 1;

    int64_t tot = 0, ans = 0;
    for (uint32_t i = 0; i < n; ++i) {
        tot += a[i];
        ans += uGet(tot - k);
        uAdd(tot);
    }
    printf("%" PRId64 "\n", ans);
}

static inline uint64_t h1(uint64_t x) {
    return x;
}

static inline void uAdd(int64_t k) {
    uint32_t t = h1(k) % M;
    for (uint32_t p = en[t]; p; p = nt[p]) {
        if (h[p] == k) {
            v[p]++;
            return;
        }
    }
    nt[pt] = en[t];
    h[pt] = k;
    v[pt] = 1;
    en[t] = pt++;
}

static inline int uGet(int64_t k) {
    for (uint32_t p = en[h1(k) % M]; p; p = nt[p]) {
        if (h[p] == k)
            return v[p];
    }
    return 0;
}


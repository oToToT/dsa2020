#pragma GCC optimize("Ofast")
#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <ctype.h>
#include <math.h>
#define N (1000000 + 5)
#define BUFFER_LEN (71223)

static inline char gc() {
    static char buf[BUFFER_LEN], *p = buf, *end = buf;
    if (p == end) {
        end = buf + read(0, buf, BUFFER_LEN);
        p = buf;
    }
    return *p++;
}
static inline void gn(uint32_t* x) {
    *x = 0;
    char c = gc();
    while ('0'>c || c>'9')
        c = gc();
    while ('0'<=c && c<='9')
        *x = *x * 10 + c - '0', c=gc();
}
static inline void gs(char* s) {
    while ((*(s++) = gc()) != '\n');
    *(s-1) = '\0';
}

char pbuf[BUFFER_LEN], *p = pbuf, *ed = pbuf + BUFFER_LEN;
static inline void pc(char c) {
    *(p++) = c;
    if (p == ed) {
        write(1, pbuf, BUFFER_LEN);
        p = pbuf;
    }
}
static inline void pflush() {
    write(1, pbuf, p - pbuf);
}


uint32_t cnt, len;
uint32_t mrg[N];
char val[N];
typedef struct Iter {
    uint32_t pid, cid, ctr;
} Iter;
static inline Iter begin() {
    static Iter rt;
    rt.pid = 0;
    rt.cid = 0;
    rt.ctr = 0;
    return rt;
}
static inline Iter end() {
    static Iter rt;
    rt.pid = mrg[1];
    rt.cid = 1;
    rt.ctr = len + 1;
    return rt;
}

static inline void swap(Iter* a, Iter* b) {
    Iter c = *a;
    *a = *b;
    *b = c;
}

static inline Iter next(Iter it) {
    if (it.cid == 1) return it;
    Iter rt;
    rt.ctr = it.ctr + 1;
    rt.cid = mrg[it.cid] ^ it.pid;
    rt.pid = it.cid;
    return rt;
}
static inline Iter prev(Iter it) {
    if (it.cid == 0) return it;
    Iter rt;
    rt.ctr = it.ctr - 1;
    rt.cid = it.pid;
    rt.pid = mrg[it.pid] ^ it.cid;
    return rt;
}
static inline Iter insert_after(Iter it, char v) {
    const int idx = cnt++;
    val[idx] = v;
    len++;

    Iter nit = next(it);
    mrg[nit.cid] ^= it.cid;
    mrg[nit.cid] ^= idx;

    mrg[it.cid] ^= nit.cid;
    mrg[it.cid] ^= idx;

    mrg[idx] = it.cid ^ nit.cid;
    return next(it);
}
static inline Iter remove_range(Iter st, Iter ed) {
    if (st.ctr > ed.ctr)
        swap(&st, &ed);
    len -= ed.ctr - st.ctr;
    const Iter ned = next(ed);
    mrg[st.cid] = st.pid ^ ned.cid;
    mrg[ned.cid] ^= st.cid ^ ed.cid;
    return st;
}
static inline void reverse_range(Iter* st, Iter* ed) {

    if (abs(st->ctr - ed->ctr) <= 1) return;

    bool swapped = false;
    if (st->ctr > ed->ctr) {
        swap(st, ed);
        swapped = true;
    }

    const Iter nst = next(*st);
    const Iter ned = next(*ed);
    const int nnstid = mrg[nst.cid] ^ nst.pid;

    mrg[nst.cid] ^= st->cid;
    mrg[nst.cid] ^= ned.cid;

    mrg[st->cid] ^= nst.cid;
    mrg[st->cid] ^= ed->cid;

    mrg[ed->cid] ^= ned.cid;
    mrg[ed->cid] ^= st->cid;

    mrg[ned.cid] ^= ed->cid;
    mrg[ned.cid] ^= nst.cid;

    ed->cid = nst.cid;
    ed->pid = nnstid;

    if (swapped) swap(st, ed);
}
static inline void print_list(Iter st) {
    for (st = next(st); st.cid != 1; st = next(st))
        pc(val[st.cid]);
    pc('\n');
}

size_t n;
char s[N];

static inline void init() {
    gs(s);
    n = strlen(s);
    cnt = 2;
    mrg[0] = 1;
    mrg[1] = 0;
    len = 0;
}
static inline void solve() {
    Iter pt = begin(), spt;
    bool selection = 0;
	for (size_t i = 0; i < n; ++i) {
        if (islower(s[i])) {
            // overwrite selection and exit
            if (selection) {
                pt = remove_range(spt, pt);
            }
            pt = insert_after(pt, s[i]);
            selection = false;
        } else if (s[i] == 'H') {
            // move left
            pt = prev(pt);
        } else if (s[i] == 'L') {
            // move right
            if (pt.ctr < len) {
                pt = next(pt);
            }
        } else if (s[i] == 'I') {
            // to begining
            pt = begin();
        } else if (s[i] == 'A') {
            // to end
            pt = prev(end());
        } else if (s[i] == 'V') {
            // toggle selection
            if (!selection) spt = pt;
            selection = !selection;
        } else if (s[i] == 'D') {
            // remove and exit if in selection
            if (selection && pt.ctr != spt.ctr) {
                pt = remove_range(spt, pt);
                selection = false;
            }
        } else if (s[i] == 'R') {
            // reverse and stay if in selection
            if (selection) {
                reverse_range(&spt, &pt);
            }
        }
    }
    print_list(begin());
}

int main() {
    uint32_t t; gn(&t);
    while (t--) {
        init();
        solve();
    }
    pflush();
    return 0;
}

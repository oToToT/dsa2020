#include <stdio.h>
#define N (2000000 + 5)

typedef struct node {
    int pre, nxt, val;
} node;
node nodes[N];

int node_;
int new_node(int x) {
    ++node_;
    nodes[node_].val = x;
    nodes[node_].pre = 0;
    nodes[node_].nxt = 0;
    return node_;
}
int tl[N], ed[N];

int main() {
    int n, m, q;
    scanf("%d%d%d", &n, &m, &q);
    for (int i = 1; i <= m; ++i) {
        int s; scanf("%d", &s);
        for (int j = 0; j < s; ++j) {
            int x; scanf("%d", &x);
            int id = new_node(x);
            if (tl[i]) {
                nodes[tl[i]].nxt = id;
                nodes[id].pre = tl[i];
                tl[i] = id;
            } else {
                tl[i] = id;
                ed[i] = id;
            }
        }
    }
    while (q--) {
        int op; scanf("%d", &op);
        if (op == 1) {
            int x, p;
            scanf("%d%d", &x, &p);
            int id = new_node(x);
            if (tl[p]) {
                nodes[tl[p]].nxt = id;
                nodes[id].pre = tl[p];
                tl[p] = id;
            } else {
                tl[p] = id;
                ed[p] = id;
            }
        } else if(op == 2) {
            int p;
            scanf("%d", &p);
            if (tl[p] == ed[p]) {
                tl[p] = 0;
                ed[p] = 0;
            } else {
                nodes[nodes[tl[p]].pre].nxt = 0;
                tl[p] = nodes[tl[p]].pre;
            }
        } else if (op == 3) {
            int p1, p2;
            scanf("%d%d", &p1, &p2);
            if (!ed[p1]) continue;
            if (tl[p2]) {
                nodes[tl[p2]].nxt = ed[p1];
            } else {
                ed[p2] = ed[p1];
            }
            nodes[ed[p1]].pre = tl[p2];
            tl[p2] = tl[p1];
            tl[p1] = 0;
            ed[p1] = 0;
        }
    }
    for (int i = 1; i < N; ++i) {
        if (!tl[i]) continue;
        printf("%d:", i);
        for (int p = tl[i]; p; p = nodes[p].pre)
            printf(" %d", nodes[p].val);
        puts("");
    }
    return 0;
}

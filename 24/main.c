#include <stdio.h>
#define N (5000000 + 1)

unsigned F[N];

static inline void kmp(const char s[], const int n) {
    F[0] = 0;
    for (unsigned i = 1, j = 0; i < n; ++i) {
        while (j && s[i] != s[j])
            j = F[j - 1];
        if (s[i] == s[j]) ++j;
        F[i] = j;
    }
}

char s[N];

int main() {
    unsigned n; scanf("%u", &n);
    scanf("%s", s);
    kmp(s, n);
    for (unsigned i = 0; i < n; ++i) {
        unsigned p = i + 1 - F[i];
        if ((i + 1) % p != 0) p = i + 1;
        printf("%d: %u\n", i + 1, p);
    }
    return 0;
}


#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

static inline char gc() {
    static char buf[100000], *p = buf, *end = buf;
    if (p == end) {
        end = buf + read(0, buf, 100000);
        p = buf;
    }
    return *p++;
}
static inline void gn(int* x) {
    *x = 0;
    register char c = gc();
    while ('0'>c || c>'9')
        c = gc();
    while ('0'<=c && c<='9')
        *x = *x * 10 + c - '0', c=gc();
}

#define N 1123456
#define M (N << 1)

int hd[N], to[M];
int nxt[M], eid = 1;

static inline void add_edge(int u, int v) {
    nxt[eid] = hd[u];
    hd[u] = eid;
    to[eid] = v;
    ++eid;
}

void quick_sort(int b[], int n) {
    if (n <= 10) {
        for (int i = 0; i < n - 1; ++i) {
            for (int j = 0; j < n - i - 1; ++j) {
                if (b[j] > b[j + 1]) {
                    b[j] ^= b[j + 1];
                    b[j + 1] ^= b[j];
                    b[j] ^= b[j + 1];
                }
            }
        }
        return;
    }
    int p = b[n >> 1];
    int i = 0, j = n;
    while (i < j) {
        while (b[i] < p) i++;
        while (b[j - 1] > p) j--;
        if (i >= j - 1) break;
        b[i] ^= b[j - 1];
        b[j - 1] ^= b[i];
        b[i] ^= b[j - 1];
        ++i, --j;
    }
    quick_sort(b, i);
    quick_sort(b + i, n - i);
}


void quick_sort64(int64_t b[], int n) {
    if (n <= 10) {
        for (int i = 0; i < n - 1; ++i) {
            for (int j = 0; j < n - i - 1; ++j) {
                if (b[j] > b[j + 1]) {
                    b[j] ^= b[j + 1];
                    b[j + 1] ^= b[j];
                    b[j] ^= b[j + 1];
                }
            }
        }
        return;
    }
    int64_t p = b[n >> 1];
    int i = 0, j = n;
    while (i < j) {
        while (b[i] < p) i++;
        while (b[j - 1] > p) j--;
        if (i >= j - 1) break;
        b[i] ^= b[j - 1];
        b[j - 1] ^= b[i];
        b[i] ^= b[j - 1];
        ++i, --j;
    }
    quick_sort64(b, i);
    quick_sort64(b + i, n - i);
}



int v[N];
int64_t ct[N];

void dfs(int u, int p) {
    for (int _ = hd[u]; _; _ = nxt[_]) {
        int v = to[_];
        if (v == p) continue;
        dfs(v, u);
        ct[u] += ct[v];
    }
}

int main() {
    int n, m; gn(&n); gn(&m);
    for (int i = 0; i < m; ++i) {
        int x; gn(&x);
        ct[x] += 1;
    }
    for (int i = 2; i <= n + 1; ++i)
        gn(&v[i]);
    for (int i = 1; i < n; ++i) {
        int u, v; gn(&u); gn(&v);
        add_edge(u, v);
        add_edge(v, u);
    }
    dfs(1, 1);
    quick_sort64(ct + 2, n - 1);
    quick_sort(v + 2, n);
    int64_t ans = 0;
    for (int i = 2; i <= n; ++i)
        ans += v[i] * ct[i];
    printf("%" PRId64 "\n", ans);
    return 0;
}

#include <stdio.h>
#include <assert.h>
#define max(a, b) (((a) > (b)) ? (a) : (b))
#define N (1000000 + 5)
#define M (1000000 + 5)

static int tot[M];
static int clr[N + M];
static int pa[N + M];
static int idx[N];

static inline void swap(int *, int *);
static int query(int);

int main() {
    // input spec
    int n, q;
    scanf("%d%d", &n, &q);

    // initialize
    for (int i = 0; i <= n + q; ++i)
        pa[i] = i;
    for (int i = 1; i <= n; ++i)
        idx[i] = i;
    tot[0] = n;

    int ctr = 1, cid = n;
    while (q--) {
        char op[2];
        scanf("%s", op);
        if (op[0] == 'w') {
            int x; scanf("%d", &x);
            int u = query(idx[x]);
            tot[clr[u]]--;
            tot[ctr] = 1;
            if (clr[u] == 0) {
                clr[u] = ctr;
            } else {
                idx[x] = ++cid;
                clr[idx[x]] = ctr;
            }
            ctr++;
        } else if (op[0] == 'u') {
            int x, y;
            scanf("%d%d", &x, &y);
            int u = query(idx[x]);
            int v = query(idx[y]);
            int c1 = clr[u], c2 = clr[v];
            if (c1 == c2) continue;
            if (c1 == 0) {
                pa[u] = v;
                tot[c1]--;
                tot[c2]++;
            } else if (c2 == 0) {
                pa[v] = u;
                tot[c2]--;
                tot[c1]++;
            } else {
                if (c1 < c2) swap(&c1, &c2);
                pa[u] = v;
                tot[c1] += tot[c2];
                tot[c2] = 0;
                clr[v] = c1;
            }
        } else if (op[0] == 'h') {
            int x; scanf("%d", &x);
            tot[clr[query(idx[x])]]--;
            tot[0]++;
            idx[x] = ++cid;
        } else if (op[0] == 'a') {
            int x; scanf("%d", &x);
            printf("%d\n", clr[query(idx[x])]);
        } else if (op[0] == 'n') {
            int x; scanf("%d", &x);
            printf("%d\n", tot[x]);
        }
    }
    return 0;
}

static int query(int x) {
    return (pa[x] == x) ? x : (pa[x] = query(pa[x]));
}

static inline void swap(int *x, int *y) {
    int t = *x;
    *x = *y;
    *y = t;
}

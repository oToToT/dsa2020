#include <stdio.h>
#include <stdbool.h>
#define N 50000
#define M 1200000
#define K 10

bool inq[N];
bool aval[N][K];
int hd[N], to[M + 1];
int nxt[M + 1], eid = 1;

static inline void add_edge(int u, int v) {
    nxt[eid] = hd[u];
    hd[u] = eid;
    to[eid] = v;
    ++eid;
}

int qu[N], qt, qe;

int main() {
    int n, m, k;
    scanf("%d%d%d", &n, &m, &k);
    for (int i = 0; i < m; ++i) {
        int u, v; scanf("%d%d", &u, &v);
        add_edge(u, v);
        /*add_edge(v, u);*/
    }
    qu[qe++] = 0;
    inq[0] = true;
    aval[0][0] = true;
    while (qt != qe) {
        int u = qu[qt++];
        inq[u] = false;
        if (qt == N) qt = 0;
        for (int _ = hd[u]; _; _ = nxt[_]) {
            int v = to[_];
            bool ok = false;
            for (int i = 0; i < k - 1; ++i) {
                if (!aval[u][i]) continue;
                if (aval[v][i + 1]) continue;
                aval[v][i + 1] = true;
                ok = true;
            }
            if (aval[u][k - 1] && !aval[v][0]) {
                aval[v][0] = true;
                ok = true;
            }
            if (ok && !inq[v]) {
                qu[qe++] = v;
                inq[v] = true;
                if (qe == N) qe = 0;
            }
        }
    }
    bool ok = true;
    for (int i = 0; i < n; ++i) {
        if (aval[i][0]) {
            printf("%s%d", ok ? "" : " ", i);
            ok = false;
        }
    }
    puts("");
    return 0;
}

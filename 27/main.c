#include <stdio.h>
#include <stdlib.h>
#define N (20000 + 5)

int cmp(const void * a, const void * b) {
   return (*(int*)a) - (*(int*)b);
}

int nxt[N], to[N], hd[N], fa[N];
int a[N], r[N];

void f(int,int);
int* dfs(int,int,int*);

int main() {
    int n; scanf("%d", &n);
    for (int i = 1; i <= n; ++i)
        scanf("%d", &a[i]);
    for (int i = 1; i < n; ++i) {
        int u, v; scanf("%d%d", &u, &v);
        nxt[i << 1 | 0] = hd[u];
        to[i << 1 | 0] = v;
        hd[u] = i << 1 | 0;

        nxt[i << 1 | 1] = hd[v];
        to[i << 1 | 1] = u;
        hd[v] = i << 1 | 1;
    }
    f(1, 1);
    int q; scanf("%d", &q);
    while (q--) {
        int u, k;
        scanf("%d%d", &u, &k);
        int *rr = dfs(u, fa[u], r);
        qsort(r, rr - r, sizeof(int), cmp);
        /*for (int *t=r; t!=rr;++t)*/
            /*printf("%d ", *t);*/
        /*puts("");*/
        printf("%d\n", r[k - 1]);
    }
    return 0;
}

void f(int u, int p) {
    fa[u] = p;
    for (int v_ = hd[u]; v_; v_ = nxt[v_]) {
        if (to[v_] == p) continue;
        f(to[v_], u);
    }
}

int* dfs(int u, int p, int *x) {
    /*printf("meow: %d\n", u);*/
    *(x++) = a[u];
    for (int v_ = hd[u]; v_; v_ = nxt[v_]) {
        if (to[v_] == p) continue;
        x = dfs(to[v_], u, x);
    }
    return x;
}

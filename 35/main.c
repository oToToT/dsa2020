#include <stdio.h>
#include <inttypes.h>
typedef uint64_t llu;
#define N 10000000
#define L (1000000 + 5)
#define P 127
#define Q 12553019

uint32_t en[Q];
int pt = 1;
int nt[N];
int v[N];
llu h[N];

char s[L];
void uAdd(llu);

int main() {
    int n; scanf("%d", &n);
    for (int i = 0; i < n; ++i) {
        scanf("%s", s);
        llu h = 0;
        for (int t = 0; s[t] != '\0'; ++t)
            h = h * P + s[t];
        uAdd(h);
    }
    int mx = 0;
    for (int i = 0; i < N; ++i) {
        if (mx < v[i]) mx = v[i];
    }
    printf("%d\n", mx);
    return 0;
}

void uAdd(llu k) {
    uint32_t t = k % Q;
    for (uint32_t p = en[t]; p; p = nt[p]) {
        if (h[p] == k) {
            v[p]++;
            return;
        }
    }
    nt[pt] = en[t];
    h[pt] = k;
    v[pt] = 1;
    en[t] = pt++;
}

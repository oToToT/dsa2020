#include <stdio.h>
#include <stdbool.h>

#define N 512345
#define M (N << 1)

int hd[N], nxt[M];
int to[M], eid = 1;

static inline void add_edge(int u, int v) {
    nxt[eid] = hd[u];
    to[eid] = v;
    hd[u] = eid++;
}

int a[N];

int pq[N], pq_;

static inline void swap(int *x, int *y) {
    int t = *x;
    *x = *y;
    *y = t;
}

void push(int x) {
    pq[++pq_] = x;
    for (int t = pq_; t > 1; t >>= 1) {
        int h = t >> 1;
        if (a[pq[t]] >= a[pq[h]])
            break;
        swap(&pq[t], &pq[h]);
    }
}

int pop() {
    int r = pq[1];
    pq[1] = pq[pq_--];
    for (int t = 1; t * 2 <= pq_;) {
        int s = t * 2;
        if (s + 1 <= pq_ && a[pq[s + 1]] < a[pq[s]])
            s += 1;
        if (a[pq[t]] <= a[pq[s]]) break;
        swap(&pq[t], &pq[s]);
        t = s;
    }
    return r;
}

static inline int peek() { return pq[1]; }

static inline int size() { return pq_; }

int qu[N], qs, qt;

static inline void enqueue(int x) {
    qu[qt++] = x;
    if (qt == N) qt = 0;
}

static inline int dequeue() {
    int r = qu[qs++];
    if (qs == N) qs = 0;
    return r;
}

bool vis[N];

int main() {
    int n, m;
    scanf("%d%d", &n, &m);
    for (int i = 0; i < m; ++i) {
        int u, v; scanf("%d%d", &u, &v);
        add_edge(u, v);
        add_edge(v, u);
    }
    for (int i = 1; i <= n; ++i)
        scanf("%d", &a[i]);
    int s, t; scanf("%d%d", &s, &t);
    push(s);
    int ans = 0;
    while (size()) {
        int x = pop();
        if (x != s) ans = a[x];
        enqueue(x);
        while (size() && a[peek()] <= ans) {
            vis[peek()] = true;
            enqueue(pop());
        }
        bool done = false;
        while (qs != qt) {
            int u = dequeue();
            if (u == t) {
                done = true;
                break;
            }
            for (int _ = hd[u]; _; _ = nxt[_]) {
                int v = to[_];
                if (vis[v]) continue;
                if (a[v] > a[u]) {
                    push(v);
                } else {
                    vis[v] = true;
                    enqueue(v);
                }
            }
        }
        if (done) break;
    }
    printf("%d\n", ans);
    return 0;
}

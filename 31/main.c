#include<stdio.h>
#include<stdlib.h>
#define MAXSIZE 100000
#define RRcycle 1

enum Policy {
    FIFO,
    RR,
    SJF,
    PSJF
}; 
enum Policy policy;

typedef struct dish {
    int id, readyTime, remainingTime;
    int duration;       // how much time a certain dish is eaten continuously
    int isRunning;      // whether the dish is eaten or not
} dish;

typedef struct Heap {
    int size;
    dish *arr;
} heap;

/*  returns the parent of the i-th element in a 0-base binary heap  */
int parent(int i) {
    return (i - 1) / 2;
}

/*  returns the left child of the i-th element in a 0-base binary heap  */
int left(int i) {
    return i * 2 + 1;
}

/*  returns the right child of the i-th element in a 0-base binary heap  */
int right(int i) {
    return i * 2 + 2;
}

/*  If arr[a] has higher priority than arr[b], return a; otherwise, return b.
    You have to determine the priority according to the given policy.  */
int cmp(dish *arr, int a, int b){
    if (policy == FIFO) {
        if (arr[a].id < arr[b].id)
            return a;
        return b;
    } else if (policy == RR) {
        if (arr[a].duration < arr[b].duration)
            return a;
        if (arr[a].duration == arr[b].duration)
            return arr[a].id < arr[b].id ? a : b;
        return b;
    } else if (policy == PSJF) {
        if (arr[a].remainingTime < arr[b].remainingTime)
            return a;
        if (arr[a].remainingTime == arr[b].remainingTime)
            return arr[a].id < arr[b].id ? a : b;
        return b;
    } else {
        if (arr[a].isRunning) return a;
        if (arr[b].isRunning) return b;
        if (arr[a].remainingTime < arr[b].remainingTime)
            return a;
        if (arr[a].remainingTime == arr[b].remainingTime)
            return arr[a].id < arr[b].id ? a : b;
        return b;
    }
}

/*  You may call the cmp function above to implement heapify according to the slides. */
void heapify(heap *h, int i) {
    while (1) {
        int chl = i;
        int l = left(i);
        if (l < h->size) {
            if (cmp(h->arr, chl, l) == l)
                chl = l;
        }
        int r = right(i);
        if (r < h->size) {
            if (cmp(h->arr, chl, r) == r)
                chl = r;
        }
        if (chl == i) break;
        dish t = h->arr[i];
        h->arr[i] = h->arr[chl];
        h->arr[chl] = t;
        i = chl;
    }
}

/*  Enque a dish d into the h. 
    You may call the cmp function above to implement enqueue according to the slides. */
void enqueue(heap *h, dish d) {
    h->arr[h->size] = d;
    for (int i = h->size; i > 0; i = parent(i)) {
        int j = parent(i);
        if (cmp(h->arr, i, j) != i)
            break;
        dish t = h->arr[i];
        h->arr[i] = h->arr[j];
        h->arr[j] = t;
    }
    h->size++;
}

/*  You may call the cmp function above to implement pop according to the slides. */
void pop(heap *h) {
    h->arr[0] = h->arr[--(h->size)];
    heapify(h, 0);
}

/* New a heap with h->arr points to the arr in the parameter */
heap *newHeap(int size, dish *arr) {
    heap* h = (heap *)malloc(sizeof(heap));
    h->size = size;
    h->arr = arr;
    for (int i = parent(size - 1); i >= 0; i--) {
        heapify(h, i);
    }
    return h;
}

dish arr[MAXSIZE];
int main() {
    int n, readyTime, remainingTime, finishedNum = 0, time = 0, lastSwitch = 0;
    char s[1000];

    scanf("%s%d", s, &n);
    if (s[0] == 'F') policy = FIFO;
    else if (s[0] == 'R') policy = RR;
    else if (s[0] == 'S') policy = SJF;
    else policy = PSJF;

    for (int i = 0; i < n; i++) {
        scanf("%d%d", &readyTime, &remainingTime);
        arr[i].id = i;
        arr[i].remainingTime = remainingTime;
        arr[i].readyTime = arr[i].duration = readyTime;
        arr[i].isRunning = 0;
    }

    //Make a heap and insert the dishes with ready time = 0 into it.
    for (; finishedNum < n && arr[finishedNum].readyTime == 0; finishedNum++);
    heap *priorityQueue = newHeap(finishedNum, arr);
    dish *root = &priorityQueue->arr[0];

    lastSwitch = root->readyTime;
    root->isRunning = 1;
    while (finishedNum < n || priorityQueue->size != 0) {
        //insert ready dishes to priorityQueue 
        for (; finishedNum < n && arr[finishedNum].readyTime == time; finishedNum ++) {
            enqueue(priorityQueue, arr[finishedNum]);
        }

        if (priorityQueue->size != 0) {
            if (root->remainingTime == 0) {
                printf("%d %d\n", root->id, time);
                pop(priorityQueue);
                lastSwitch = time;
            } else if (policy == RR && time - lastSwitch == RRcycle) {
                root->duration = time;
                heapify(priorityQueue, 0);
                lastSwitch = time;
            }
            root->isRunning = 1;
            root->remainingTime--;
        }
        time++;
    }
    return 0;
}

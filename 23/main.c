#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#define N (1000000 + 5)
#define M (100000 + 5)
#define K (3000000 + 5)
#define P 127
#define Q 1002939109

#define min(x, y) (((x) < (y)) ? (x) : (y))

static inline char gc() {
    static char buf[99824], *p = buf, *end = buf;
    if (p == end) {
        end = buf + read(0, buf, 99824);
        p = buf;
    }
    return *p++;
}
static inline void gn(int* x) {
    *x = 0;
    register char c = gc();
    while ('0'>c || c>'9')
        c = gc();
    while ('0'<=c && c<='9')
        *x = *x * 10 + c - '0', c=gc();
}
static inline void gs(char* s) {
    while ((*(s++) = gc()) != '\n');
    *(s-1) = '\0';
}

static inline int mul(int64_t a, int64_t b) {
    return a * b % Q;
}
static inline int add(int a, int b) {
    return a + b >= Q ? a + b - Q : a + b;
}
static inline int sub(int a, int b) {
    return a - b < 0 ? a - b + Q : a - b;
}

char s[N];
int h[N], pw[N];

char ans[K];
int ans_len;
int g[K];

int main() {
    gs(s + 1);
    
    int n = strlen(s + 1);
    for (int i = n; i >= 1; --i)
        h[i] = add(mul(h[i + 1], P), s[i]);
    pw[0] = 1;
    for (int i = 1; i < N; ++i)
        pw[i] = mul(pw[i - 1], P);
    
    int m; gn(&m);
    while(m--) {
        int l, r;
        gn(&l); gn(&r);
        
        int rr = min(r, ans_len + l - 1);
        for (int cur_len = rr - l + 1; cur_len >= 0; cur_len--) {
            // check [l, r] != [saved_len - r + l, saved_len]
            const int gv = sub(g[ans_len], mul(g[ans_len - cur_len], pw[cur_len]));
            const int hv = sub(h[l], mul(h[l + cur_len], pw[cur_len]));
            if (gv == hv) break;
            rr--;
        }
        
        // complete hash
        while (rr++ < r) {
            ans[++ans_len] = s[rr];
            g[ans_len] = add(mul(g[ans_len - 1], P), ans[ans_len]);
        }
    }
    write(1, ans + 1, ans_len);
    return 0;
}

#include <stdio.h>
#include <string.h>
#define N (8000 + 5)
#define M (8000 * 1000 + 5)
#define C (26)
#define INF (1 << 30)

typedef struct node {
    int sub[C];
    int a, b;
} node;
node nodes[M];
int nid;
static inline int newnode() {
    nodes[nid].a = nodes[nid].b = INF;
    return nid++;
}

static inline int idx(char c) {
    return c - 'a';
}

static inline void insert(int r, char s[], int a, int b) {
    if (b < nodes[r].b) {
        nodes[r].a = a;
        nodes[r].b = b;
    }
    if (s[0] == '\0') return;
    if (!nodes[r].sub[idx(s[0])]) {
        nodes[r].sub[idx(s[0])] = newnode();
    }
    insert(nodes[r].sub[idx(s[0])], s + 1, a, b + 1);
}

static inline int query(int r, char s[]) {
    if (r == 0) return r;
    if (s[0] == '\0') return r;
    return query(nodes[r].sub[idx(s[0])], s + 1);
}

char s[N];

int main() {
    // init
    nid = 1;
    int rt = newnode();

    // build
    int n, q; scanf("%d%d", &n, &q);
    for (int i = 0; i < n; ++i) {
        scanf("%s", s);
        int m = strlen(s);
        for (int j = 0; j < m; ++j)
            insert(rt, s + j, i, j - 1);
    }

    // solve
    while (q--) {
        scanf("%s", s);
        int idx = query(rt, s);
        if (idx == 0) puts("DNA not found");
        else {
            printf("%d %d\n", nodes[idx].a, nodes[idx].b);
        }
    }
    return 0;
}
